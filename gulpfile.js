/**
 *	Gulp App Builder Task Runner
 *	@author Nicolas Pulido <nicolas.pulido@crazycake.cl>
 */

//import libs
var browserify   = require('browserify');
var babelify     = require('babelify');
var gulp         = require('gulp');
var source       = require('vinyl-source-stream');
var buffer       = require('vinyl-buffer');
var fs           = require("fs");
var watchify     = require('watchify');
var shell        = require('gulp-shell');
var assign       = require('lodash.assign');
//plugins
var watch        = require('gulp-watch');
var gutil        = require('gulp-util');
var chmod        = require('gulp-chmod');
var rename       = require("gulp-rename");
var insert       = require('gulp-insert');
var autoprefixer = require('gulp-autoprefixer');
var sass         = require('gulp-sass');
var css_minifier = require('gulp-cssmin');
var livereload   = require('gulp-livereload');
var htmlmin      = require('gulp-html-minifier');
var concat       = require('gulp-concat');
var vueify       = require('vueify');

/** Configurations **/

// sass foundation paths
var sass_paths = [
    //bootstrap sass files
    './node_modules/bootstrap-sass/assets/stylesheets',
    //font awesome
    './node_modules/font-awesome/scss'
];

//log
gutil.log(gutil.colors.blue('Executing tasks...'));

//set assets paths
var jsfiles_public_path = './dist/js/';
var cssfiles_public_path = './dist/css/';
var sass_src_files = './src/scss/';
var js_src_files = './src/js/';
var src_files = './src/';

// set up the browserify instance on a task basis
var opts = assign({}, watchify.args, {
    entries      : [ js_src_files + 'app.js'],
    cache        : {},
    packageCache : {}
});

/** Browserify setup **/
var b = watchify(browserify(opts).transform(vueify).transform(babelify, {presets: ["es2015"]})); //watchify

//options
b.on('update', bundleApp);  //on any dep update, runs the bundler
b.on('log', gutil.log);     //output build logs to terminal

function bundleApp() {
    //browserify js bundler
    return b.bundle()
        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(gulp.dest( jsfiles_public_path ))
        .pipe(livereload());
}

/**  Gulp Tasks **/

//Watch tasks
gulp.task('watch', function() {
    gutil.log(gutil.colors.green('Watching scss or js source files changes...'));

    //live reaload
    livereload.listen();
    //js bundle
    bundleApp();
    //sass files
    gulp.watch(sass_src_files + "**/*.scss", ['compile-sass-app']);
    // html + vue files
    gulp.watch([src_files + "**/*.html", src_files + "**/*.vue"], ['minify-html']);
});

//Sass app compiler
gulp.task('compile-sass-app', function() {
    return gulp.src([sass_src_files + 'app.scss'])
    .pipe(sass({ includePaths: sass_paths })
          .on('error', sass.logError))
    .pipe(autoprefixer({
        browsers : ['last 2 versions'],
        cascade  : false
    }))
    .pipe(concat('app.css'))
    .pipe(gulp.dest(cssfiles_public_path))
    .pipe(livereload());
});

//CSS minifier
gulp.task('minify-css', function() {

    gutil.log(gutil.colors.yellow('Minifing css files...'));
    return gulp.src([sass_src_files + "*.css", "!" + sass_src_files + "*.*.css"])
        .pipe(buffer())
        .pipe(css_minifier())
        .pipe(rename({ suffix : ".min" }))
        .pipe(chmod(775))
        .pipe(gulp.dest(cssfiles_public_path));
});

//JS minfier (gulp-uglify is slow)
gulp.task('minify-js', shell.task([
    "uglifyjs " + js_src_files.replace("./","") + "app.js" + " -o " + js_src_files.replace("./","") + "app.min.js"
]));

//HTML minifier
gulp.task('minify-html', function() {
    gulp.src('./src/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./dist'))
        .pipe(livereload());
});

//Build & Deploy
gulp.task('build', ['minify-css', 'minify-js'], function() {
    //minification done
    gutil.log(gutil.colors.blue('Minification complete.'));
});