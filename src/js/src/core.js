/**
 * App Core: main app module.
 * Dependencies: `jQuery.js`,`lodash.js`.
 * Required scope vars: `{APP, UA}`.
 * Frontend Framework supported: `Foundation v.6.x`, `Bootstrap v4.x`
 * @class Core
 */

//load main libraries
//require('html5shiv');
//require('fastclick');
require('lodash');
//javascript framework
require('vue-router');
require('vue-resource');

//javascript dom manipulation
require('jquery');
require('bootstrap');
//jquery plugins
require('jquery-flot');
//require('datatables');
require('datatables.net')(window, $);
require('datatables.net-buttons')(window, $);
require('moment');

module.exports = function() {
    //self context
    var self = this;

    //++ Properties

    /**
     * @property modules
     * @type {Boolean}
     */
    self.modules = {};

    /**
     * @property window.core
     * @type {object}
     */
    window.core = self;

    //++ Methods ++

    /**
     * Set modules automatically for require function
     * @method init
     * @param {Array} modules - The required modules
     */
    self.setModules = function (modules) {

        if(!modules.length)
            return;

        for (var i = 0; i < modules.length; i++) {

            var mod = modules[i];

            if(typeof mod.moduleName !== "undefined")
                self.modules[mod.moduleName] = mod;
        }
    };

    /**
     * Core Ready Event, called automatically after loading modules.
     * @method ready
     */
    self.ready = function() {
        //Vue http resources
        Vue.use(VueResource);

        //Vue http resources
        Vue.use(VueRouter);

        //Date filter
        Vue.use(require('vue-moment'));

        //load retina images
        self.retinaImages();

        //Internal init
        self.loadModules(self.modules);
    };

    /**
     * Loads App modules, if module has a viewModel binds it to DOM automatically
     * @method loadModules
     * @param {Object} modules - The modules oject
     */
    self.loadModules = function (modules) {
        var mod_name, mod, vm, data;

        //1) load viewModels
        for (mod_name in modules) {

            //check module exists
            if(_.isUndefined(self.modules[mod_name]))
                continue;

            //get module
            mod = self.modules[mod_name];

            //bind model to DOM?
            if(!_.isObject(mod.vm))
                continue;

            vm = _.assign({
                //vue element selector
                el : '#vue-' + mod_name
            }, mod.vm);

            //if(APP.dev) { console.log("App Core -> Binding " + mod_name + " View Model", vm); }

            //set new Vue instance (object prop updated)
            mod.vm = new Vue(vm);
        }

        //2) call inits
        for (mod_name in modules) {
            //check module exists
            if(_.isUndefined(self.modules[mod_name])) {
                console.warn("App Core -> Attempting to load an undefined view module ("+mod_name+").");
                //continue;
            }

            //get module
            mod  = self.modules[mod_name];
            data = modules[mod_name];

            //check if module has init method & call it
            if(_.isFunction(mod.init)){
                mod.init(data);
                console.log('mod init :'+mod_name)
            }
        }
    };

    /**
     * Redirect router method
     * TODO: detect protocol schema.
     * @method redirectTo
     * @param  {String} uri - The webapp URI
     */
    self.redirectTo = function(uri) {

        var uri_map = {
            notFound : "error/notFound"
        };

        //check if has a uri map
        if(!_.isUndefined(uri_map[uri]))
            uri = uri_map[uri];

        //redirect to contact
        location.href = APP.baseUrl + uri;
    };

    /**
     * Validates screen size is equal to given size.
     * @TODO: check screen size with Bootstrap
     * @method checkScreenSize
     * @param  {String} size - The Screen size: small, medium, large.
     * @return {Boolean}
     */
    self.checkScreenSize = function(size) {
        //bootstrap
        if(self.framework == "bootstrap") {

            if ($(window).width() < 768)
                return size === "small";
            else if ($(window).width() >= 768 && $(window).width() <= 992)
                return size === "medium";
            else if ($(window).width() > 992 && $(window).width() <= 1200)
                return size === "large";
            else
                return size === "x-large";
        }

        return false;
    };

    /**
     * Toggle Retina Images for supported platforms.
     * @method retinaImages
     * @param  {Object} context - A jQuery element context (optional)
     */
    self.retinaImages = function(context) {

        //check if client supports retina
        var isRetina = function() {

            var mediaQuery = '(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)';

            if (window.devicePixelRatio > 1)
                return true;

            if (window.matchMedia && window.matchMedia(mediaQuery).matches)
                return true;

            return false;
        };

        if(!isRetina()) return;

        //get elements
        var elements = (typeof context != "undefined") ? $('img[data-retina]', context) : $('img[data-retina]');

        //for each image with attr data-retina
        elements.each(function() {

            var obj = $(this);

            var src = obj.attr("src");
            var ext = src.slice(-4);
            //set new source
            var new_src = src.replace(ext, "@2x"+ext);

            obj.removeAttr("data-retina");
            obj.attr("src", new_src);
        });
    };

    /**
     * Image preloader, returns an array with image paths [token replaced: '$']
     * @method preloadImages
     * @param  {String} image_path - The source path
     * @param  {Int} indexes - The indexes, example: image1.png, image2.png, ...
     * @return {Array} The image object array
     */
    self.preloadImages = function(image_path, indexes) {

        if(_.isUndefined(indexes) || indexes === 0)
            indexes = 1;

        var objects = [];

        //preload images
        for(var i = 0; i < indexes; i++) {
            //create new image object
            objects[i] = new Image();
            //if object has a '$' symbol replace with index
            objects[i].src = image_path.replace('$', (i+1));
        }

        return objects;
    };
};